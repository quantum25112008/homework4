import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Введите число:");
        int number = scanner.nextInt();

        if (number >= 10 ){
            System.out.println("Положительное число больше 10 или равно 10");
        }else if (number <= 0){
            System.out.println("Отрицательное число или число равно 0");
        }
    }
}